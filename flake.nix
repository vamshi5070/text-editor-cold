{
  inputs = {
    # nixpkgs.url = "nixpkgs";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        libraries = with pkgs;[
          webkitgtk
          gtk3
          cairo
          gdk-pixbuf
          glib
          dbus
          openssl_3
          glib-networking
        ];

        packages = with pkgs; [
          curl
          wget
          pkg-config
          dbus
          openssl_3        
          glib-networking
          glib
          gtk3
          libsoup
          webkitgtk
         # nodejs-19_x
          nodePackages_latest.create-react-app
#          nodePackages_latest.npm
          nodePackages_latest.pnpm
          cargo
          rustc
          # haskellPackages.ghcup
          nodePackages_latest.prettier
          nodePackages_latest.eslint
          nodePackages_latest.typescript
          #elm
        ];
      in
      {
        devShell = pkgs.mkShell {
          buildInputs = packages;

          shellHook =
            ''
              export GIO_MODULE_DIR=${pkgs.glib-networking}/lib/gio/modules/
              export LD_LIBRARY_PATH=${pkgs.lib.makeLibraryPath libraries}:$LD_LIBRARY_PATH
            '';
        };
      });
}
