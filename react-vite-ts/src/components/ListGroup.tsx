//import {Fragment} from "react";
import {MouseEvent} from "react";
import {useState} from "react";

interface Props {
  items: string;
  heading: string;
  onSelectItem: (item: string) => void;
}

function ListGroup({items, heading, onSelectItem}: Props) {

  /*  let items = [
    'New York',
    'San Francisco',
    'Tokyo',
    'London',
    'Paris'
  ];
*/
  //  let selectedIndex = 0;
  const [selectedIndex,setSelectedIndex] = useState(-1);
  const getMessage = () => {
   // return items.length === 0 ? <p> No item found </p> : null;
   return items.length === 0 && <p> No item found </p> ;
  }
  const handleClick = (event: MouseEvent) => console.log(event);
      return (
    <>
      <h1>List</h1>
      {getMessage()}
    <ul className="list-group">
      
    {items.map((item, index) => (<li className={selectedIndex === index ? 'list-group-item active' : 'list-group-item '} key={item} onClick={() => {
        setSelectedIndex(index);
        onSelectItem(item);
        }}>{item}</li>))}
   </ul>
    </>
  );
}

export default ListGroup;

/*
  <li className="list-group-item">An item</li>
  <li className="list-group-item">A second item</li>
  <li className="list-group-item">A third item</li>
  <li className="list-group-item">A fourth item</li>
  <li className="list-group-item">And a fifth one</li>
*/