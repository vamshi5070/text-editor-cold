//import Message from './Message';
import ListGroup from "./components/ListGroup";
import {useEffect} from 'react';

function KApp(){

  let items = [
    'New York',
    'San Francisco',
    'Tokyo',
    'London',
    'Paris'
  ];

  const handleSelectItem = (item:string) => {
    console.log(item);
  }
//   const name = '';
//   if (name) 
//    return <h1> Hello {name}</h1>;
//   else
  	return <div><ListGroup items={items} heading="Cities" onSelectItem={handleSelectItem}/></div>;
}

function App(){
  const url = "https://192.168.64.3:8081/todos"
  //"https://jsonplaceholder.typicode.com/comments"

  useEffect(() => {
    const fetchPosts = async () => {
      try {
        const response = await fetch('https://localhost:8081/todos');
        const data = await response.json();
        console.log(data);
        setPosts(data);
      } catch (error) {
        console.error(error);
      }
    };

    fetchPosts();
  }, []);
  useEffect(() => {
    fetch(url)
     .then(response => {
      console.log("response",response)
    }).catch(e => {
      console.log("e",e)
    })
  })
}

export default App;

/*
import { usestate } from 'react'
import reactlogo from './assets/react.svg'
import vitelogo from '/vite.svg'
import './app.css'

function app() {
  const [count, setcount] = usestate(0)

  return (
    <div classname="app">
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src={vitelogo} classname="logo" alt="vite logo" />
        </a>
        <a href="https://reactjs.org" target="_blank">
          <img src={reactlogo} classname="logo react" alt="react logo" />
        </a>
      </div>
      <h1>vite + react</h1>
      <div classname="card">
        <button onclick={() => setcount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          edit <code>src/app.tsx</code> and save to test hmr
        </p>
      </div>
      <p classname="read-the-docs">
        click on the vite and react logos to learn more
      </p>
    </div>
  )
}
export default app
*/
