import logo from './logo.svg';
import './App.css';
import React, { useEffect , useState} from 'react';
import ReactDOM from 'react-dom';


function App() {
const root = ReactDOM.createRoot(document.getElementById('root'));

    const [data, setData] = useState([]);
   useEffect(() => {
    fetch('http://192.168.64.3:8081/todos')
      .then(response => response.json())
      .then(jsonData => {
        // Handle the response data here
          // console.log(data);
	  setData(jsonData);
      })
      .catch(error => {
          // Handle any errors here
        console.error(error);
      });
  }, []);

    root.render(
 <h1>Hello, world!</h1> <>

      <ul>
          {data.map(item => (
          <li key={item.todoId}>{item.task}</li>
        ))}
      </ul>
    );
};
 /*
	<input
            id="new-todo"
            onChange={this.handleChange}
            value={this.state.text}
          />

class HelloMessage extends React.Component {
  render() {
    return <div>Hello {this.props.name}</div>;
  }
}
data.map(item => (
        <div key={item.todoId}>
          <h2>{item.task}</h2>
          <p>{item.completed}</p>
        </div>
     ))
	
    root.render(<HelloMessage name="Taylor" />);

function App() {
    const [inputValue, setInputValue] = useState('');

    function handleInputChange(event) {
	setInputValue(event.target.value);
    }
  function handleKeyDown(event) {
    if (event.key === 'Enter') {
      setInputValue(event.target.value);
    }
  }

  return (
    <div className="App">
      <header className="App-header">
     
    <div>
      <input type="text" value={inputValue} onKeyDown={handleKeyDown} />
      <p>You typed: {inputValue}</p>
    </div>
 
      </header>
    </div>

  );
}
*/
export default App;

