// Add an event listener to the form
document.querySelector('form').addEventListener('submit', function(event) {
	event.preventDefault(); // Prevent the form from submitting

	// Get the todo input value
	const todoInput = document.querySelector('#todo');
	const todoValue = todoInput.value.trim();
	todoInput.value = ''; // Clear the input field

	// Create a new list item and add it to the todo list
	const todoList = document.querySelector('#todo-list');
	const newTodo = document.createElement('li');
	newTodo.textContent = todoValue;
	todoList.appendChild(newTodo);
});

function toggleDark(){
  const element = document.body;
  element.classList.toggle("dark-mode");
}
