import { useState, useEffect } from "react";
import handleAddButtonClick from "./PostTodo";
import handleDeleteButtonClick from "./DeleteTask";
//import styles from '@/styles/Home.module.css'

export default function AllTodo() {
    const [textInput, setTextInput] = useState("");
    const [firstTime, setFirstTime] = useState(false);
    //  const [postData, setPostData] = useState([]);
    const [lastData, setLastData] = useState([]);
    const [isChecked, setIsChecked] = useState(false);
    const [isEditable, setIsEditable] = useState(false);
    const [editId, setEditId] = useState(-1);
    const [editContent, setEditContent] = useState("");

    const handleCheckboxChange = () => {
	setIsChecked(!isChecked);
    };
    
    const handleEditChange = () => {
	setIsEditable(!isEditable);
    };

    function handleEditId(currentId) {
	setEditId(currentId);
    }
    function changeToEdit(currentId) {
	handleEditChange();
	handleEditId(currentId);
	//    console.log(currentId);
    }
    function handleTextInputChange(event) {
	setTextInput(event.target.value);
    }

    function handleEditTextInputChange(event) {
	setEditContent(event.target.value);
    }
    async function handlePatchButtonClick(tJson,event) {
	event.preventDefault();
	const response = await fetch("http://192.168.64.3:8081/todos", {
	    method: "PATCH",
	    headers: {
		"Content-Type": "application/json",
	    },
	    body: JSON.stringify(tJson),
	});
	const json = await response.json();
	//    setPostData(json);
	return (json);
	//    console.log(json);
    }
    
    async function handlePutButtonClick(tJson,event) {
	event.preventDefault();
	if (editContent === "") {
	    return lastData;
	}
	else {
	    const updatedObject = {
		taskWithId: editContent,
		todoId: tJson.todoId,
		completedWithId: tJson.completedWithId
	    };
	    const response = await fetch("http://192.168.64.3:8081/todos", {
		method: "PUT",
		headers: {
		    "Content-Type": "application/json",
		},
		body: JSON.stringify(updatedObject),
	    });
	    const json = await response.json();
	    return (json);
	}
    }
    async function fetchData(){
	const response = await fetch('http://192.168.64.3:8081/todos');
	const data = await response.json();
	//      return data;
	return (data);
    }
    async function updateFetchData() {
	const data = await fetchData();
	setLastData(data);
    }
    async function updatePostData(event) {
	if (textInput == "") {
	    return;
	} else {
	    const data = await handleAddButtonClick(event,textInput);
	    setLastData(data);
	    setTextInput('');
	}
    }
    async function updatePatchData(tJson,event) {
	const data = await handlePatchButtonClick(tJson,event);
	setLastData(data);
    }
    async function updateEditData(tJson,event) {
	const data = await handlePutButtonClick(tJson,event);
	handleEditChange();
	setLastData(data);
	setEditContent('');
    }
    async function updateDeleteData(tId) {
	const data = await handleDeleteButtonClick(tId);
	setLastData(data);
    }
    if (!firstTime){
	updateFetchData();
	setFirstTime(true);
	//   await setLastData(fetchData());
    }
    //  else 
    //    console.log("")
    const incompletedItems = lastData.filter(item => !item.completedWithId);
    const completedItems = lastData.filter(item => item.completedWithId);
    //  console.log(completedItems);
    // handleAddButtonClick
    // onChange={handleCheckboxChange}/>
    // onChange={handleCheckboxChange}
    //{item.completedWithId} 
    //item.taskWithId
    //             {item.taskWithId} : 
    //            {if isEditable {item.taskWithId} else <input type="text" value={textInput} onChange={handleTextInputChange}  />}
    // className = "bg-pink"
    	//className="text-black bg-white border-2 border-green-600" />

    return (
	<>
	    <input type="text" value={textInput} onChange={handleTextInputChange} type="text" placeholder="Add a new task here" className="input input-bordered w-full max-w-xs"/>
	    <button className="btn btn-primary w-full" onClick={updatePostData}>Add</button>
	    <h2 className="text-2xl font-bold"> InCompleted items </h2>
	    <ul>
	    {incompletedItems.map((item) => (
		<li key={item.todoId}>
		    <input type="checkbox" checked={false}  onChange={(event) => updatePatchData(item,event)}/>
		    {isEditable && (item.todoId === editId) ? <> 
			<input type="text" defaultValue={item.taskWithId}
		     onChange= {handleEditTextInputChange}/>
			<button className="btn" onClick={() => updateEditData(item,event)}>Save</button> 
			</> : <> {item.taskWithId}   <button className="btn btn-secondary" onClick={() => changeToEdit(item.todoId)}>Edit</button> </>}
		    <button className="btn" onClick={() => updateDeleteData(item.todoId)}>Delete</button> 
		    </li>        
            ))}
	</ul>
	    <h2 className="text-2xl font-bold"> Completed items </h2>
	    <ul>
	    {completedItems.map((item) => (
		<li key={item.todoId}>
		    <input type="checkbox" checked={item.completedWithId} onChange={(event) => updatePatchData(item,event)}/>
		    {item.taskWithId} 
		    <button className="bg-blue-500 rounded text-white"  onClick={() => updateDeleteData(item.todoId)}>Delete</button>
		    </li>        
            ))}
	</ul>
	    </>
    );
    //  const lastData = async () => {
    /*
      if (postData.length === 0) {
      const data = fetchData();
      setLastData(data);
      }
      else {
      setLastData(postData);
      }
      );
      /*
      if (postData.length === 0){
      return <>
      Loading 
      </>;
      }
      else
      return (
      <>
      <input type="text" value={textInput} onChange={handleTextInputChange}/>
      <button  onClick={handleAddButtonClick}>Add</button>
      <h1> Usha parinayam</h1>
      </>
      );
    */
}
