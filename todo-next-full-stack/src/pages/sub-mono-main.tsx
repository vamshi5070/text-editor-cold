import { useState, useEffect } from "react";

export default function AllTodo() {
  const [textInput, setTextInput] = useState("");
//  const [postData, setPostData] = useState([]);
  const [lastData, setLastData] = useState([]);

  async function handleDeleteButtonClick(tId) {
//    event.preventDefault();
    const response = await fetch('http://192.168.64.3:8081/todos', {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(tId)
    });
    const json = await response.json();
    return (json);
  //  router.refresh();
 }
  function handleTextInputChange(event) {
    setTextInput(event.target.value);
  }
  async function handleAddButtonClick(event,tInput) {
    event.preventDefault();
    const response = await fetch("http://192.168.64.3:8081/todos", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(tInput),
    });
    const json = await response.json();
//    setPostData(json);
    return (json);
//    console.log(json);
  }
  async function fetchData(){
      const response = await fetch('http://192.168.64.3:8081/todos');
      const data = await response.json();
//      return data;
      return (data);
  }
  async function updateFetchData() {
  const data = await fetchData();
  setLastData(data);
}

  async function updatePostData(event) {
  const data = await handleAddButtonClick(event,textInput);
  setLastData(data);
  setTextInput('');
}
  
  async function updateDeleteData(tId) {
  const data = await handleDeleteButtonClick(tId);
  setLastData(data);
}
  if (lastData.length === 0){
    updateFetchData();
//   await setLastData(fetchData());
    }
  else 
    console.log("rgv")
  const incompletedItems = lastData.filter(item => !item.completedWithId);
  const completedItems = lastData.filter(item => item.completedWithId);
  console.log(completedItems);
// handleAddButtonClick
  return (
    <>
      <input type="text" value={textInput} onChange={handleTextInputChange}  />
      <button  onClick={updatePostData}>Add</button>
      <h2> InCompleted items </h2>
      <ul>
      {incompletedItems.map((item) => (
          <li key={item.todoId}>
            <input type="checkbox"/>
            {item.taskWithId} 
            <button onClick={() => updateDeleteData(item.todoId)}>Delete</button>
          </li>        
        ))}
      </ul>
      <h2> Completed items </h2>
      <ul>
      {completedItems.map((item) => (
          <li key={item.todoId}>
            <input type="checkbox"/>
            {item.taskWithId} 
            <button  onClick={() => updateDeleteData(item.todoId)}>Delete</button>
          </li>        
        ))}
      </ul>
      <h2> Misc </h2>
      <li> Launch factorial video with audio</li>
      <li> Launch Map,filter video with audio</li>
    </>
  );
//  const lastData = async () => {
  /*
    if (postData.length === 0) {
      const data = fetchData();
      setLastData(data);
    }
    else {
      setLastData(postData);
      }
  );
/*
  if (postData.length === 0){
    return <>
       Loading 
      </>;
  }
  else
  return (
    <>
      <input type="text" value={textInput} onChange={handleTextInputChange}/>
      <button  onClick={handleAddButtonClick}>Add</button>
      <h1> Usha parinayam</h1>
    </>
  );
*/
}
