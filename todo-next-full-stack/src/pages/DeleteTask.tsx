export default  async function handleDeleteButtonClick(tId) {
//    event.preventDefault();
    const response = await fetch('http://192.168.64.3:8081/todos', {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(tId)
    });
    const json = await response.json();
    return (json);
  //  router.refresh();
 }
