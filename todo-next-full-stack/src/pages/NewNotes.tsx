import { useState, useEffect } from "react";

export default function PostNotes() {
  const [headingInput, setHeadingInput] = useState("");
  const [contentInput, setContentInput] = useState("");

  function handleHeadingInputChange(event) {
    setHeadingInput(event.target.value);
  }
  function handleContentInputChange(event) {
    setContentInput(event.target.value);
  }

 async function handleAddButtonClick(event,hInput,cInput) {
    event.preventDefault();
    const updatedObject = {
      heading: hInput,
      content: cInput,
//      completedWithId: completeNew
    };
    const response = await fetch("http://192.168.64.3:8083/newNotes", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(updatedObject),
    });
//    const json = await response.json();
//    setPostData(json);
//    return (json);
//    console.log(json);
  }
  async function updatePostData(event) {
    const data = await handleAddButtonClick(event,headingInput,contentInput);
//    setHeadingData(data);
//    setContentData(data);
    setHeadingInput('');
    setContentInput('');
  }

  return (
    <>
    <div className="flex flex-row">
    <h1 className="text-2xl"> Heading </h1>
    </div>
    <input value={headingInput} onChange={handleHeadingInputChange} className="border-2 border-green-600 h-32 w-1/2 " />
    <div className="flex flex-row">
    <h1 className="text-2xl"> Content </h1>
    </div>
    <textarea value={contentInput} onChange={handleContentInputChange} type="Text" className="border-2 border-green-600 h-72 w-1/2 " />
    <button onClick={updatePostData} className="rounded bg-green-600 text-4xl text-white border" >Add</button>
    </>
  );
}

/*

    <button className="rounded bg-green-600 text-xl text-white border" >Add</button>
    <input type="text" value={} onChange={}/>
*/