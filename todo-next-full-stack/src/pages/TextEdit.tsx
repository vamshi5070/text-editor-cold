import { useState, useEffect } from "react";


export default function TextEdit() {
    const [textContent, setTextContent] = useState("");
    const [firstTime, setFirstTime] = useState(true);

    async function fetchData(){
	const response = await fetch('http://192.168.64.3:8081/todos');
	const data = await response.json();
	//      return data;
	return (data);
    }

    async function updateFetchData() {
	const data = await fetchData();
	setTextContent(data.content);
	setFirstTime(false);
    }

    if (firstTime){
	updateFetchData();
	// setFirstTime(true);
    }
    return (
	<>
	    <textarea type="text" defaultValue={textContent} className="border-2 border-green-600  h-screen w-3/4" />

	        <button className="rounded bg-green-600 text-4xl text-white border" >Add</button>

	    </>
	    
    )
}
//value={textInput} onChange={handleTextInputChange}  />
