//"use client";
import {useRouter} from "next/navigation";
import {useState,useEffect } from 'react';
//import TodoList from "./"

const [textInput, setTextInput] = useState('');
const [data,setData] = useState([]);

async function handleAddButtonClick(event) {
      event.preventDefault();
      const response = await fetch('http://192.168.64.3:8081/todos', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(textInput)
      });
        const json = await response.json();
        setData(json);
        console.log(json);
    };

function AddNewTodo(){
  const router = useRouter();

  function handleTextInputChange(event) {
    setTextInput(event.target.value);
  }

        //}, []);
//    });

//    router.refresh();
// }
  const completedItems = data.filter(item => item.completedWithId)
  const incompletedItems = data.filter(item => !item.completedWithId)
  console.log(completedItems);
  
  return(
    <>
      <input type="text" value={textInput} onChange={handleTextInputChange}  />
      <button  onClick={handleAddButtonClick}>Add</button>
      <h2> InCompleted items </h2>
      <ul>
      {incompletedItems.map((item) => (
          <li key={item.todoId}>
            <input type="checkbox"/>
            {item.taskWithId} 
            "tem"
            <button>Delete</button>
          </li>        
        ))}
      </ul>
      <h2> Completed items </h2>
      <ul>
      {completedItems.map((item) => (
          <li key={item.todoId}>
            <input type="checkbox"/>
            {item.taskWithId} 
            <button>Delete</button>
          </li>        
        ))}
      </ul>
    </>
  );
}
const MY_CONSTANT = 43;
export {MY_CONSTANT};
export default AddNewTodo;

/*
    setTextInput('');
    const result = await response.json();
    console.log(result);
    */
