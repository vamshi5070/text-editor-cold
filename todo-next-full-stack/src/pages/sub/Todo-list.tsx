//const getTodos = async () => {
//  let todos = await fetch("http://example.com/mydata");
  //fetch("http://localhost:8081/todos");
//  return todos.json();
//}
//import 
import {useState, useEffect } from 'react';
import {useRouter} from "next/navigation";
import {MY_CONSTANT} from "./AddTodoTask";

export default function TodoList() {
  //const { todos } = await getTodos();
  const [data,setData] = useState([]);
  const router = useRouter();

  useEffect(() => {
    async function fetchData(){
      const response = await fetch('http://192.168.64.3:8081/todos');
      const json = await response.json();
      setData(json);
    }
    fetchData();  
  },[] );

  if (!data){
    return <>Loading...</>;
  }
  
  async function handleDeleteButtonClick(tId) {
//    event.preventDefault();
    const response = await fetch('http://192.168.64.3:8081/todos', {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(tId)
    });
    router.refresh();
 }
  
  const completedItems = data.filter(item => item.completedWithId)
  const incompletedItems = data.filter(item => !item.completedWithId)
  /*
  */
  //console.log(todos);
  return (
    <>
      <h1> {MY_CONSTANT} </h1>
      <h2> InCompleted items </h2>
      <ul>
      {incompletedItems.map((item) => (
          <li key={item.todoId}>
            <input type="checkbox"/>
            {item.taskWithId} 
            <button onClick={() => handleDeleteButtonClick(item.todoId)}>Delete</button>
          </li>        
        ))}
      </ul>
      <h2> Completed items </h2>
      <ul>
      {completedItems.map((item) => (
          <li key={item.todoId}>
            <input type="checkbox"/>
            {item.taskWithId} 
            <button  onClick={() => handleDeleteButtonClick(item.todoId)}>Delete</button>
          </li>        
        ))}
      </ul>
      <h2> Misc </h2>
      <li> Launch factorial video with audio</li>
      <li> Launch Map,filter video with audio</li>
    </>
  );
}
