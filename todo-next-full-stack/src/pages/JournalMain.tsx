import { useState, useEffect } from "react";
export default function JournalHeadings() {
  const [lastData, setLastData] = useState([]);
async function fetchData(){
      const response = await fetch('http://192.168.64.3:8083/notesHeadings');
      const data = await response.json();
//      return data;
      return (data);
}
  async function updateFetchData() {
    const data = await fetchData();
    setLastData(data);
  }
  if (lastData.length === 0){
    updateFetchData();
//   await setLastData(fetchData());
    }
    //updateFetchData();
//const data = fetchData ();
  return (
    <>
      <ul className="text-5xl ">
      {lastData.map((item) => (
         <div className="flex flex-row space-x-4">  <li className= "border " key={item.noteId}> {item.headingWithId} </li> 
            <button className="bg-green-300"> Edit </button>
            </div>
      ))}
      </ul>
    </>
  );
}