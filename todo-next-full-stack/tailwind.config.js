/** @type {import('tailwindcss').Config} */

const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  content: [
      // Or if using `src` directory:
    "./src/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
       fontFamily: {
        sans: ['Inter var', ...defaultTheme.fontFamily.sans],
        mono: ['Fira Code VF', ...defaultTheme.fontFamily.mono],
        source: ['Source Sans Pro', ...defaultTheme.fontFamily.sans],
        'ubuntu-mono': ['Ubuntu Mono', ...defaultTheme.fontFamily.mono],
      },
      maxWidth: {
        '8xl': '90rem',
      },
    extend: {     
      spacing: {
        18: '4.5rem',
        full: '100%',
      },
      fontSize: {
        xs: '12px',
        sm: '15px',
        base: '18px',
      },
    },
  },
  plugins: [require("daisyui")],
}

