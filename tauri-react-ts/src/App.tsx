import "./App.css";
import {useState,useEffect} from 'react';
//import axios from 'axios';


function App(){
  const [data,setData] = useState([]);

  useEffect (() => {
    async function fetchData(){
      const response = await fetch('http://192.168.64.3/8081/todos');
      const json = await response.json();
      setData(json);
    }
    fetchData();
  }, [] );
  if (!data){
    return <>Loading</>;
  }
  return (
    <>
      <ul>
      {data.map((item) => (
          <li key={item.todoId}>
            {item.taskWithId} 
          </li>        
        ))}
      <li> Launch factorial video with audio</li>
      <li> Launch Map,filter video with audio</li>
      </ul>
    </>
  );
}

/*
function kApp(){
  useEffect (() => {
    fetch(url)
      .then(response => {
      console.log("response",response)
    }).catch(e => {
      console.log("error",e)
    })
  }, [])
}
function App(){
  useEffect(() => {
//    const url = "https://192.168.64.3:8081/todos"
  const url = "https://jsonplaceholdder.typicode.com/comments"
    axios.get(url).then((response) => {
      console.log(response.data);
    });
  },[]);
  console.log('rgv');
}
*/



export default App;