import { useState, useEffect } from 'react';

interface Post {
  todoId: number;
  taskWithId: string;
  completedWithId: bool;
//  body: string;
}

export default function  App(){
  function HomeScreen() {
  const [posts, setPosts] = useState<Post[]>([]);

  useEffect(() => {
    const fetchPosts = async () => {
      try {
        const response = await fetch('https://localhost:8081/todos');
        const data = await response.json();
        console.log(data);
        setPosts(data);
      } catch (error) {
        console.error(error);
      }
    };

    fetchPosts();
  }, []);
    return (
      <>
        {posts.map(post => (
          <div>
            <h2>{post.taskWithId}</h2>
          </div>
        ))      
      }
      </>
    );
  }
  return <HomeScreen/>;
}

//        <h1>Todos</h1>
