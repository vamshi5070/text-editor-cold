import { useState } from "react";
//import "./styles.css";
import "./App.css";

function App() {
  const [screen, setScreen] = useState("Screen1");

  function handleScreen1Click() {
    setScreen("Screen1");
  }

  function handleScreen2Click() {
    setScreen("Screen2");
  }

  return (
    <div className="App">
      {screen === "Screen1" ? (
        <Screen1 onButtonClick={handleScreen2Click} />
      ) : (
        <Screen2 onButtonClick={handleScreen1Click} />
      )}
    </div>
  );
}

function Screen1({ onButtonClick }) {
  return (
    <div className="container">
      <h1>Screen 1</h1>
      <button onClick={onButtonClick}>Go to Screen 2</button>
    </div>
  );
}

function Screen2({ onButtonClick }) {
  return (
//    <div>
    <div className="container">
      <h1>Screen 2</h1>
      <button onClick={onButtonClick}>Go to Screen 1</button>
    </div>
  );
}

